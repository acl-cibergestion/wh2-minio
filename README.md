# ACL-Cibergestion #

Servicio intermediario entre el API Core MinIO & Microservicios Restful

### Para que es este repositorio? ###

Este servicio es actua como interlocutor entre llamdas http/rest y el API MinIO
Version: 1


### Configuraciones ###

Para el correcto funcionamiento debe existir previamente en el ambiente un MinIO Tenant corriendo bajo las siguientes
espesificaciones generales:
1. Debe estar bajo el mismo ambiente de red Kubernetes
2. Debe responder al siguiente URI: https://minio-tenant-1-hl.minio-tenant-1.svc.cluster.local:9000

### Una vez desplegado: Entrar al pod para configurar el certificado. 
1- Abrir la consola del kubernetes.
kubectl get pod -n backend
2-(Copiar el pod del minio -> wh2-minio-5754789cdd-b4849)
kubectl exec -it wh2-minio-5754789cdd-b4849 -n backend -- /bin/bash
3-Comprobar que estamos dentro del pod con el comando ls
apt-get update
apt-get install vim
vi file.sh
a
4- Copiar el contenido del archivo que se encuentra en la carpeta cert del microservicio para el ambiente donde se este ejecutando.
esc
:
wq
enter
chmod +x file.sh
./file.sh

### Operations Expuestas 
5
### -- uploadFile --  
Permite subir cualquier tipo de fichero a un bucket existente.
### Example request
Method=POST, URL=http://localhost:8080/minio/uploadFile
### BODY JSON
{
  "bucketName": "cibergestionbucket",
  "pathInputFile": "C:\\Users\\Liliana\\Desktop\\ClaseInglesBasico.txt",
  "inputFileName": "ClaseInglesBasico.txt"
}
### Example response
### Response Header
content-type: application/json
### Response body
{
"operationStatus": true,
"operationMsg": "Procesado"
}

### -- bucketExist --	 
Verifica si existe un determinado bucket
### Example request
Method=GET, URL=http://localhost:8080/minio/bucketExist?bucketName=miniobucket
### Example response
### Response Header
content-type: application/json
### Response body
{
"operationStatus": true,
"operationMsg": "Existe"
}


### -- getFile	--
Permite obtener un link para descargar un fichero contenido en un bucket
### Example request
Method=GET, URL=http://localhost:8080/minio/getFile?bucketName=miniobucket&fileName=hola.txt&numExpirySeconds=60
### Example response
### Response Header
content-type: application/json
### Response body
{
"operationStatus": true,
"operationMsg": "Archivo obtenido: hola.txt OK",
"urlFile": "https://minio-tenant-1-hl.minio-tenant-1.svc.cluster.local:9000/miniobucket/hola.txt?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=admin%2F20211122%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Date=20211122T005433Z&X-Amz-Expires=60&X-Amz-SignedHeaders=host&X-Amz-Signature=01940731021d628b40704a092c7b8c39209f777e57ce3aaa54e190770ce437fe"
}

### -- getListObjt --	 
Lista todos los ficheros contenidos en un determinado bucket
### Example request
Method=GET, URL=http://localhost:8080/minio/getListObjt?bucketName=cibergestionbucket
### Example response
### Response Header
content-type: application/json
### Response body
{
"operationStatus": true,
"operationMsg": "Lista recibida OK",
"listObjt": [
  {
		"name": "ClaseInglesBasico.txt",
		"size": "18785",
		"lastModified": "2021-11-22T00:53:12.779Z"
}
],
}

### -- getListBucket -- 
Lista todos lo buckets existentes.
### Example request
Method=GET, URL=http://localhost:8080/minio/getListBucket
### Example response
### Response Header
content-type: application/json
### Response body
{
"operationStatus": true,
"operationMsg": "Lista recibida OK",
"listBucket": [
  {
"name": "cibergestionbucket",
"creationDate": "2021-11-20T21:42:32.694Z"
},
  {
"name": "miniobucket",
"creationDate": "2021-11-15T19:33:59.996Z"
}
],
}


###Nota.Si desea conectar de forma interna al cluster use el internal balanceador interno del fichero: wh2-minio-interno-lb.yaml