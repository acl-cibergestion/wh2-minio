#!/usr/bin/env sh

KEYSTORE_FILE=$JAVA_HOME/lib/security/cacerts
KEYSTORE_PASS=changeit


import_cert() {
  local HOST=$1
  local PORT=$2

  if [[ -z $PORT ]]; then
    PORT=443
  fi

  # get the SSL certificate
  openssl s_client -connect ${HOST}:${PORT} </dev/null | sed -ne '/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p' > ${HOST}.cert

  # delete the old alias and then import the new one
  keytool -delete -keystore ${KEYSTORE_FILE} -storepass ${KEYSTORE_PASS} -alias ${HOST} &> /dev/null

  # create a keystore (or update) and import certificate
  keytool -import -noprompt -trustcacerts \
      -alias ${HOST} -file ${HOST}.cert \
      -keystore ${KEYSTORE_FILE} -storepass ${KEYSTORE_PASS}

  # remove temp file
  rm ${HOST}.cert
}

import_cert minio.minio-tenant-core.svc.cluster.local 443