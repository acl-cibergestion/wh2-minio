package cl.workflow.clientminio;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClientminioApplication {

	public static void main(String[] args) {
		SpringApplication.run(ClientminioApplication.class, args);
	}

}
