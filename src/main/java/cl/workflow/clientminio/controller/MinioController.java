package cl.workflow.clientminio.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import cl.workflow.clientminio.service.*;
import org.springframework.web.bind.annotation.*;

import cl.workflow.clientminio.model.*;

import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

@RestController
@RequestMapping("/minio")
public class MinioController {
	
	private final Logger LOGGER = LoggerFactory.getLogger(MinioController.class);
	
	@Autowired
	private MinioService minioService;
	
	@PostMapping("/uploadFileRemote")
	@ResponseBody
	public RespMinioGeneric uploadFileRemote(@RequestHeader("bucketName") String bucketName, @RequestParam("file") MultipartFile file){
		
		LOGGER.info("MinioController.uploadFileRemote:\n\t bucketName=" + bucketName + ", nameFile=" + file.getOriginalFilename() );
		
		RespMinioGeneric response = new RespMinioGeneric();
		String fileLocation = "";
		if (file.isEmpty()) {
          
			response.setOperationStatus(false);
			response.setOperationMsg("El fichero esta sin datos");
            return response;
        }

        try {
			InputStream in = file.getInputStream();
			File currDir = new File(".");
			String path = currDir.getAbsolutePath();
			fileLocation = path.substring(0, path.length() - 1) + file.getOriginalFilename();
			
			LOGGER.info("location: "+fileLocation);
			FileOutputStream f = new FileOutputStream(fileLocation);
			int ch = 0;
			while ((ch = in.read()) != -1) {
				f.write(ch);
			}
			f.flush();
			f.close();
			
			LOGGER.info("message You successfully uploaded '" + file.getOriginalFilename() + "...'");		 
				 
        } catch (Exception e) {
            response.setOperationStatus(false);
			response.setOperationMsg( "Error al subir fichero, en etapa 1: " + e.getMessage() );
            return response;
        }    
		
		return minioService.uploadFile( bucketName, fileLocation,  file.getOriginalFilename() );
	}
	
	@PostMapping("/uploadFile")
	@ResponseBody
	public RespMinioGeneric uploadFile(@RequestBody ReqMinioUploadFile reqMinioUploadFile){
		
		LOGGER.info("MinioController.uploadFile:\n\t bucketName=" + reqMinioUploadFile.getBucketName() + ", pathInputFile=" + reqMinioUploadFile.getPathInputFile() + ", inputFileName=" + reqMinioUploadFile.getInputFileName());
		return minioService.uploadFile( reqMinioUploadFile.getBucketName(),  reqMinioUploadFile.getPathInputFile(),  reqMinioUploadFile.getInputFileName());
	}
	
	@GetMapping("/bucketExist")
	@ResponseBody
	public RespMinioGeneric bucketExist(String bucketName){		
		LOGGER.info("MinioController.bucketExist:\n\t bucketName=" + bucketName);
		return minioService.bucketExist( bucketName );
	}
	
	
	@GetMapping("/getFile")
	@ResponseBody
	public RespMinioFile getFile(String bucketName, String fileName, int numExpirySeconds){		
		LOGGER.info("MinioController.getFile:\n\t bucketName=" + bucketName + ", fileName: " + fileName + ", numExpirySeconds: " + numExpirySeconds);
		return minioService.getFile( bucketName,  fileName, numExpirySeconds);
	}
	
	@GetMapping("/getListObjt")
	@ResponseBody
	public RespMinioListObjt getListObjt(String bucketName)
	{		
		LOGGER.info("MinioController.getListObjt");
		return minioService.getListObjt( bucketName);
	}
	
	@GetMapping("/getListBucket")
	@ResponseBody
	public RespMinioListBucket getListBucket() {
		
		LOGGER.info("MinioController.getListBucket");
		return minioService.getListBucket();
	}
	
}