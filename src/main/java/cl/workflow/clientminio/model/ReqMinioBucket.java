package cl.workflow.clientminio.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class ReqMinioBucket {
	
	private String bucketName;
	
}