package cl.workflow.clientminio.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ReqMinioUploadFile {
	
	private String bucketName;
	private String pathInputFile;
	private String inputFileName;

}