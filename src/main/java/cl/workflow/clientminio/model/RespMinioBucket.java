package cl.workflow.clientminio.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class RespMinioBucket {
	
	private String name;
	
	private String creationDate;
	
}