package cl.workflow.clientminio.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RespMinioFile {
	
	private boolean operationStatus;
	
	private String operationMsg;
	
	private String urlFile;
}