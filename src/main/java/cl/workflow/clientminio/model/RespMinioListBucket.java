package cl.workflow.clientminio.model;

import lombok.Getter;
import lombok.Setter;
import java.util.List;

@Getter
@Setter
public class RespMinioListBucket {
	
	private boolean operationStatus;
	
	private String operationMsg;
	
	private List<RespMinioBucket> listBucket;
}