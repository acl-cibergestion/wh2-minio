package cl.workflow.clientminio.model;

import lombok.Getter;
import lombok.Setter;
import java.util.List;

@Getter
@Setter
public class RespMinioListObjt {
	
	private boolean operationStatus;
	
	private String operationMsg;
	
	private List<RespMinioObjt> listObjt;
}