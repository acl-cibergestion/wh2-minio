package cl.workflow.clientminio.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RespMinioObjt {
	
	private String name;
	
	private String size;
	
	private String lastModified;
	
}