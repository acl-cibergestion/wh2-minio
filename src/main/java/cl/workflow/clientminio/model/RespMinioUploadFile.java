package cl.workflow.clientminio.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RespMinioUploadFile {
	
	private boolean operationStatus;
	private String operationMsg;
}