package cl.workflow.clientminio.repository;

import org.springframework.stereotype.Repository;
import io.minio.*;

import java.io.IOException;

import io.minio.messages.Item;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import cl.workflow.clientminio.model.*;
import io.minio.http.Method;
import java.util.*;

import io.minio.messages.Bucket;

@Repository
public class MinioRepository {

    private final Logger LOGGER = LoggerFactory.getLogger(MinioRepository.class);

    @Value("${minio.url}")
    private String minioUrl;

    @Value("${minio.user}")
    private String minioUser;

    @Value("${minio.password}")
    private String minioPassword;

    @Value("${minio.url.download}")
    private String minioUrlDownload;

    private static final String CONST_BUCKET_MSG = "The bucket: ";

    private static final String CONST_NO_EXISTE_MSG = "no existe";

    private static final String CONST_ERROR_UPLOAD_MSG = "Error in uploadFile: ";

    public RespMinioGeneric uploadFile(String bucketName, String pathInputFile, String inputFileName) {
        
    	RespMinioGeneric response = new RespMinioGeneric();
        response.setOperationStatus(false);

        LOGGER.info("MinioRepository.uploadFile");
        try {

        	MinioClient minioClient =  builderInstance(minioUrl, minioUser, minioPassword);
			boolean isExist = minioClient.bucketExists(BucketExistsArgs.builder().bucket(bucketName).build());
            if (!isExist) {
                String msgError = CONST_ERROR_UPLOAD_MSG + CONST_BUCKET_MSG + bucketName + CONST_NO_EXISTE_MSG;
                throw new IOException(msgError);
            }
            UploadObjectArgs uploadObjectArgs =  UploadObjectArgs.builder().bucket(bucketName)
			            													.object(inputFileName)
			            													.filename(pathInputFile)
			            													.build();
            
            minioClient.uploadObject(uploadObjectArgs);
            response.setOperationStatus(true);
            response.setOperationMsg(inputFileName + " -  uploaded OK");

        } catch (Exception e) {
            LOGGER.error("Error in uploadFile: " + e.getMessage());
            response.setOperationStatus(false);
            response.setOperationMsg("Falla: " + e.getMessage());
        }
       return response;
    }


    public RespMinioGeneric bucketExist(String bucketName) {

        RespMinioGeneric response = new RespMinioGeneric();
        response.setOperationStatus(false);

        LOGGER.info("MinioRepository.bucketExist");
        try {
        	
        	MinioClient minioClient =  builderInstance(minioUrl, minioUser, minioPassword);
            boolean isExist = minioClient.bucketExists(BucketExistsArgs.builder().bucket(bucketName).build());

            if (!isExist) {
                String msgError = "Error in bucketExist: " + "The bucket: " + bucketName + " no existe";
                throw new IOException(msgError);
            }

            response.setOperationStatus(true);
            response.setOperationMsg("Existe");

        } catch (Exception e) {

            LOGGER.error("Error in bucketExist: " + e.getMessage());
            response.setOperationStatus(false);
            response.setOperationMsg("Falla: " + e.getMessage());
        }
        return response;
    }


    public RespMinioFile getFile(String bucketName, String fileName, int numExpirySeconds) {

        RespMinioFile response = new RespMinioFile();
        response.setOperationStatus(false);
        LOGGER.info("MinioRepository.getFile");
        try {

        	MinioClient minioClient =  builderInstance(minioUrl, minioUser, minioPassword);
            
			String url = minioClient.getPresignedObjectUrl(
                            GetPresignedObjectUrlArgs.builder()
                                    .method(Method.GET)
                                    .bucket(bucketName)
                                    .object(fileName)
                                    .expiry(numExpirySeconds)
                                    .build());
			

            //transformacion
            String urlTransformed = url.replaceAll("//(.*?)/", "//" + minioUrlDownload + "/");

            response.setUrlFile(urlTransformed);
            response.setOperationStatus(true);
            response.setOperationMsg("Archivo obtenido: " + fileName + " OK");

        } catch (Exception e) {

            LOGGER.error("Error in getFile: " + e.getMessage());
            response.setOperationStatus(false);
            response.setOperationMsg("Falla: " + e.getMessage());
        }
        return response;
    }


    public RespMinioListObjt getListObjt(String bucketName) {

        RespMinioListObjt response = new RespMinioListObjt();
        response.setOperationStatus(false);

        LOGGER.info("MinioRepository.getListObjt");
        try {

        	MinioClient minioClient =  builderInstance(minioUrl, minioUser, minioPassword);
            boolean isExist = minioClient.bucketExists(BucketExistsArgs.builder().bucket(bucketName).build());

            if (!isExist) {
                String msgError = "Error in getListObjt: " + "The bucket: " + bucketName + " no existe";
                throw new IOException(msgError);
            }

            Iterable<Result<Item>> results =
                    minioClient.listObjects(ListObjectsArgs.builder().bucket(bucketName).build());

            List<RespMinioObjt> listObjt = new ArrayList<RespMinioObjt>();
            for (Result<Item> result : results) {
                Item item = result.get();
                RespMinioObjt obj = new RespMinioObjt(item.objectName(), item.size() + "", item.lastModified() + "");
                listObjt.add(obj);
            }

            response.setListObjt(listObjt);
            response.setOperationStatus(true);
            response.setOperationMsg("Lista recibida OK");

        } catch (Exception e) {

            LOGGER.error("Error in getListObjt: " + e.getMessage());
            response.setOperationStatus(false);
            response.setOperationMsg("Falla: " + e.getMessage());
        }
        return response;
    }

    public RespMinioListBucket getListBucket() {

        RespMinioListBucket response = new RespMinioListBucket();
        response.setOperationStatus(false);

        LOGGER.info("MinioRepository.getListBucket");
        try {

        	MinioClient minioClient =  builderInstance(minioUrl, minioUser, minioPassword);
            List<RespMinioBucket> listObjt = new ArrayList<RespMinioBucket>();
            List<Bucket> bucketList = minioClient.listBuckets();
            for (Bucket bucket : bucketList) {

                RespMinioBucket obj = new RespMinioBucket(bucket.name(), bucket.creationDate() + "");
                listObjt.add(obj);
            }

            response.setListBucket(listObjt);
            response.setOperationStatus(true);
            response.setOperationMsg("Lista recibida OK");

        } catch (Exception e) {

            LOGGER.error("Error in getListBucket: " + e.getMessage());
            response.setOperationStatus(false);
            response.setOperationMsg("Falla: " + e.getMessage());
        }

        return response;
    }
    
    private MinioClient builderInstance(String minioUrl, String minioUser, String minioPassword) {
    	
    	return  MinioClient.builder().endpoint(minioUrl)
		        .credentials(minioUser, minioPassword)
		        .build();
    }
}
