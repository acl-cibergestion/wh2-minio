package cl.workflow.clientminio.service;

import org.springframework.stereotype.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import cl.workflow.clientminio.repository.*;
import cl.workflow.clientminio.model.*;

@Service
public class MinioService {
	
	private final Logger LOGGER = LoggerFactory.getLogger(MinioService.class);
	
	@Autowired
	private MinioRepository minioRepository;
	
	
	
	public RespMinioGeneric uploadFile(String bucketName, String pathInputFile, String inputFileName){
		
		LOGGER.info("MinioService.uploadFile");
		return minioRepository.uploadFile( bucketName,  pathInputFile,  inputFileName);
	}
	
	public RespMinioGeneric bucketExist(String bucketName){
		
		LOGGER.info("MinioService.bucketExist");
		return minioRepository.bucketExist(bucketName);
	}
	
	
	public RespMinioFile getFile(String bucketName, String fileName, int numExpirySeconds){		
		LOGGER.info("MinioService.getFile");
		return minioRepository.getFile( bucketName,  fileName, numExpirySeconds);
	}
	
	public RespMinioListObjt getListObjt(String bucketName)
	{		
		LOGGER.info("MinioService.getListObjt");
		return minioRepository.getListObjt( bucketName);
	}


	public RespMinioListBucket getListBucket() {
		
		LOGGER.info("MinioService.getListBucket");
		return minioRepository.getListBucket();
	}
	
}
